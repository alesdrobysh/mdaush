import { BookmarkTreeNode } from './Manager';

const MOCKED_BOOKMARKS: BookmarkTreeNode[] = [
  {
    'dateAdded': 1521125471179,
    'dateGroupModified': 1521789419456,
    'id': '88',
    'index': 1,
    'parentId': '85',
    'title': 'Programming',
    'children': [
      {
        'dateAdded': 1520929407714,
        'id': '82',
        'index': 0,
        'parentId': '88',
        'title': 'FE Dev Handbook · GitBook',
        'url': 'https://frontendmasters.com/books/front-end-handbook/2018/'
      },
      {
        'dateAdded': 1519588996649,
        'id': '80',
        'index': 1,
        'parentId': '88',
        'title': 'How To Interview Senior JavaScript Developers and Architects',
        'url': 'https://www.logicroom.co/how-to-interview-senior-javascript-developers-and-architects/'
      },
      {
        'dateAdded': 1519218047726,
        'id': '78',
        'index': 2,
        'parentId': '88',
        'title': '7 architectural attributes of a reliable React component',
        'url': 'https://dmitripavlutin.com/7-architectural-attributes-of-a-reliable-react-component/'
      },
      {
        'dateAdded': 1516609805254,
        'id': '76',
        'index': 3,
        'parentId': '88',
        // tslint:disable-next-line
        'title': 'Собеседование для фронтенд-разработчика на JavaScript: самые лучшие вопросы / Блог компании RUVDS.com / Хабрахабр',
        'url': 'https://habrahabr.ru/company/ruvds/blog/334538/'
      },
      {
        'dateAdded': 1489139256446,
        'id': '58',
        'index': 4,
        'parentId': '88',
        'title': 'Самые полезные приёмы работы в командной строке Linux / Блог компании RUVDS.com / Хабрахабр',
        'url': 'https://habrahabr.ru/company/ruvds/blog/323330/'
      },
      {
        'dateAdded': 1493014764286,
        'id': '61',
        'index': 5,
        'parentId': '88',
        'title': 'Interactive Data Visualization for the Web',
        'url': 'http://chimera.labs.oreilly.com/books/1230000000345/index.html'
      },
      {
        'dateAdded': 1521461408600,
        'id': '97',
        'index': 6,
        'parentId': '88',
        'title': 'CodeSandbox: Online Code Editor Tailored for Web Application Development',
        'url': 'https://codesandbox.io/'
      },
      {
        'dateAdded': 1521537366060,
        'id': '100',
        'index': 7,
        'parentId': '88',
        'title': 'CodePen',
        'url': 'https://codepen.io/'
      },
      {
        'dateAdded': 1521633500714,
        'id': '103',
        'index': 9,
        'parentId': '88',
        'title': 'Большой комок грязи / Хабрахабр',
        'url': 'https://habrahabr.ru/post/351686/'
      },
      {
        'dateAdded': 1521789416697,
        'id': '104',
        'index': 10,
        'parentId': '88',
        'title': 'Introduction · TypeScript Deep Dive',
        'url': 'https://basarat.gitbooks.io/typescript/content/'
      }
    ]
  },
  {
    'dateAdded': 1521125371889,
    'dateGroupModified': 1521900669068,
    'id': '87',
    'index': 0,
    'parentId': '85',
    'title': 'Hot Links',
    'children': [
      {
        'dateAdded': 1513150134024,
        'id': '74',
        'index': 0,
        'parentId': '87',
        'title': 'Почта — Ales.Drobysh@infor.com',
        'url': 'https://outlook.office.com/owa/?realm=infor.com&exsvurl=1&ll-cc=1049&modurl=0&path=/mail/inbox'
      },
      {
        'dateAdded': 1521125578776,
        'id': '89',
        'index': 1,
        'parentId': '87',
        'title': 'iMail',
        'url': 'https://webmail.itechart-group.com/owa/'
      },
      {
        'dateAdded': 1521125657875,
        'id': '90',
        'index': 2,
        'parentId': '87',
        'title': 'vk',
        'url': 'https://vk.com/feed'
      },
      {
        'dateAdded': 1521125963705,
        'id': '93',
        'index': 3,
        'parentId': '87',
        'title': 'YouTube',
        'url': 'https://www.youtube.com/'
      },
      {
        'dateAdded': 1521136546529,
        'id': '94',
        'index': 4,
        'parentId': '87',
        'title': 'Gmail',
        'url': 'https://mail.google.com/mail/u/0/'
      },
      {
        'dateAdded': 1521205917426,
        'id': '95',
        'index': 5,
        'parentId': '87',
        'title': 'Google Hangouts',
        'url': 'https://hangouts.google.com/hangouts/_/predictix.com?authuser=0'
      },
      {
        'dateAdded': 1521440889699,
        'id': '96',
        'index': 6,
        'parentId': '87',
        'title': 'Яндекс.Музыка',
        'url': 'https://music.yandex.ru/feed'
      },
      {
        'dateAdded': 1521531059108,
        'id': '99',
        'index': 7,
        'parentId': '87',
        'title': 'Мой диск – Google Диск',
        'url': 'https://drive.google.com/drive/u/0/my-drive'
      }
    ]
  }
];

export default class ManagerMock {
  init() { /* do nothing */ }
  loadBookmarks = () => MOCKED_BOOKMARKS;
  public remove: (id: string) => Promise<void> = (_id) => Promise.resolve();
}
