export type BookmarkTreeNode = chrome.bookmarks.BookmarkTreeNode;
export type BookmarkChangesArg = chrome.bookmarks.BookmarkChangesArg;
export type BookmarkCreateArg = chrome.bookmarks.BookmarkCreateArg;

// tslint:disable-next-line: max-line-length
// https://github.com/chromium/chromium/blob/master/chrome/common/extensions/docs/examples/extensions/managed_bookmarks/background.js
const fixURL = (function () {
  // An "a" element is used to parse the given URL and build the fixed version.
  const a = document.createElement('a');
  return function (url: string | undefined) {
    // Preserve null, undefined, etc.
    if (!url) {
      return url;
    }
    a.href = url;
    // Handle cases like "google.com", which will be relative to the extension.
    if (a.protocol === 'chrome-extension:' &&
      url.substr(0, 17) !== 'chrome-extension:') {
      a.href = 'http://' + url;
    }
    return a.href;
  };
})();

export default class Manager {
  private api: typeof chrome.bookmarks;
  private tree: BookmarkTreeNode[];

  constructor() {
    this.api = chrome.bookmarks;

  }

  async init() {
    await this.getTree();
  }

  // TODO rethink it
  // @ts-ignore
  public loadBookmarks: () => BookmarkTreeNode[] = () => this.tree[0].children[1].children;

  public remove: (id: string) => Promise<void> = (id) => {
    return new Promise((resolve) => {
      this.api.remove(id, async () => {
        await this.getTree();
        resolve();
      });
    });
  }

  public removeTree: (id: string) => Promise<void> = (id) => {
    return new Promise((resolve) => {
      this.api.removeTree(id, async () => {
        await this.getTree();
        resolve();
      });
    });
  }

  public update: (id: string, changes: BookmarkChangesArg) => Promise<void> = (id, changes) => {
    return new Promise((resolve) => {
      this.api.update(id, { ...changes, url: fixURL(changes.url) }, async () => {
        await this.getTree();
        resolve();
      });
    });
  }

  public create: (changes: BookmarkCreateArg) => Promise<void> = (changes) => {
    return new Promise((resolve) => {
      this.api.create({ ...changes, url: fixURL(changes.url) }, async () => {
        await this.getTree();
        resolve();
      });
    });
  }

  private getTree() {
    return new Promise((resolve) => {
      this.api.getTree(tree => {
        this.tree = tree;
        resolve();
      });
    });
  }
}
