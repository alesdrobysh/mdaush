import * as React from 'react';
import * as ReactDOM from 'react-dom';

import App from 'src/App';
import { getPhotos, savePhotos, Photos } from 'src/localStorage';
import 'src/index.css';
import PhotoCredits from 'src/components/PhotoCredits';

function getRandomArrayElement<T>(list: T[]): T {
  return list[Math.floor((Math.random() * list.length))];
}

const M_SECONDS_IN_DAY = 86400000;

(async () => {
  ReactDOM.render(
    <App photoCredits={<PhotoCredits />} />,
    document.getElementById('root') as HTMLElement
  );

  let photos: Photos = getPhotos();

  const photosEmpty = photos.photos.length === 0;
  const photosOutdated = Date.now() - photos.timestamp > M_SECONDS_IN_DAY;
  const photosShouldBeUpdated = photosEmpty || photosOutdated;

  if (photosShouldBeUpdated) {
    const w = window.innerWidth;
    const h = window.innerHeight;
    const photosNumber = 30;

    const res = await fetch(
      'https://api.unsplash.com/photos/random?'
      + `client_id=${process.env.REACT_APP_CLIENT_ID}&w=${w}&h=${h}&count=${photosNumber}&query=landscape`
    );

    const data = await res.json();
    photos = {
      photos: data,
      timestamp: Date.now(),
    };
    savePhotos(photos);
  }

  const randomPhoto = getRandomArrayElement(photos.photos);
  const { user } = randomPhoto;
  const { custom } = randomPhoto.urls;
  const image = new Image();

  image.src = custom;

  image.onload = () => {
    const background = document.querySelector('.background') as HTMLElement;
    background.style.backgroundImage = `url(${custom})`;
    background.classList.toggle('background-loaded');
  };

  ReactDOM.render(
    <App photoCredits={<PhotoCredits user={user} />} />,
    document.getElementById('root') as HTMLElement
  );
})();
