export interface Photo {
  urls: {
    custom: string;
  };
  user: {
    first_name: string;
    last_name: string | null;
    links: {
      html: string;
    };
  };
}

export interface Photos {
  photos: Photo[];
  timestamp: number;
}

export const getPhotos = (): Photos => {
  const emptyPhotos = {
    photos: [],
    timestamp: Date.now(),
  };

  const photos = localStorage.getItem('photos');
  if (!photos) {
    return emptyPhotos;
  }

  try {
    return JSON.parse(photos);
  } catch (e) {
    console.error(e);
    return emptyPhotos;
  }
};

export const savePhotos = (photos: Photos) => localStorage.setItem('photos', JSON.stringify(photos));
