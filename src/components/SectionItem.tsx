import * as React from 'react';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import MoreVert from '@material-ui/icons/MoreVert';
import Modal from '@material-ui/core/Modal';
import { withStyles, WithStyles, Theme } from '@material-ui/core/styles';

import { BookmarkTreeNode, BookmarkChangesArg } from 'src/api/Manager';
import EditBoormark from './EditBookmark';

const styles = (theme: Theme) => ({
  card: {
    padding: theme.spacing(1),
    margin: theme.spacing(1),
    '&:hover': {
      backgroundColor: theme.palette.grey['100'],
    },
    position: 'relative' as 'relative',
    display: 'flex',
    justifyContent: 'space-between',
  },
  img: {
    height: `${theme.spacing(2)}px`,
    width: `${theme.spacing(2)}px`,
    float: 'left' as 'left',
    margin: `${theme.spacing(1 / 4)}px ${theme.spacing(1)}px ${theme.spacing(1 / 4)}px 0`,
  },
  a: {
    textDecoration: 'none',
    flexGrow: 1,
    color: theme.palette.getContrastText(theme.palette.background.paper)
  },
  more: {
    alignSelf: 'flex-start',
    margin: `-${theme.spacing(1 / 4)}px`,
    padding: theme.spacing(1 / 4),
    position: 'relative' as 'relative',
    right: `-${theme.spacing(3 / 4)}px`,
  },
  moreInner: {
    height: `${theme.spacing(10 / 4)}px`,
    width: `${theme.spacing(10 / 4)}px`,
  }
});

export interface OwnProps {
  item: BookmarkTreeNode;
  remove: (id: string) => Promise<void>;
  update: (id: string, changes: BookmarkChangesArg) => Promise<void>;
}

type ClassKey = 'card' | 'img' | 'a' | 'more' | 'moreInner';

export function Section({ item, classes, remove, update }: OwnProps & WithStyles<ClassKey>) {
  const [anchorEl, setAnchorEl] = React.useState<Element | null>(null);
  const [isEditOpened, setIsEditOpened] = React.useState(false);

  const onMoreClick = (e: React.SyntheticEvent) => setAnchorEl(e.currentTarget);
  const onClose = () => setAnchorEl(null);
  const onEditClick = () => {
    setIsEditOpened(true);
    onClose();
  };
  const onEditModalClose = () => setIsEditOpened(false);
  const onEditModalSave = ({ title, url }: BookmarkChangesArg) => update(item.id, { title, url });
  const onDeleteClick = async () => {
    await remove(item.id);
    onClose();
  };

  return (
    <React.Fragment>
      <Paper className={classes.card}>
        <a
          href={item.url}
          className={classes.a}
        >
          <img
            src={`http://www.google.com/s2/favicons?domain=${item.url}`}
            alt={item.title}
            className={classes.img}
          />
          <Typography>{item.title}</Typography>
        </a>
        <IconButton
          aria-label="Bookmark Menu"
          aria-owns={anchorEl ? 'simple-menu' : undefined}
          aria-haspopup="true"
          className={classes.more}
          onClick={onMoreClick}
        >
          <MoreVert className={classes.moreInner} />
        </IconButton>
        <Menu
          id="card-menu"
          anchorEl={anchorEl as HTMLElement}
          open={Boolean(anchorEl)}
          onClose={onClose}
        >
          <MenuItem onClick={onEditClick}>Edit</MenuItem>
          <MenuItem onClick={onDeleteClick}>Delete</MenuItem>
        </Menu>
      </Paper>

      <Modal
        open={isEditOpened}
        onClose={onEditModalClose}
      >
        <EditBoormark
          item={item}
          onClose={onEditModalClose}
          onSubmit={onEditModalSave}
        />
      </Modal>
    </React.Fragment>
  );
}

export default withStyles(styles)(Section);
