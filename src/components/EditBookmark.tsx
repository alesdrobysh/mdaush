import * as React from 'react';
import {
  Paper,
  withStyles,
  WithStyles,
  Theme,
  FormControl,
  FormLabel,
  Typography,
  InputLabel,
  Input,
  Button,
} from '@material-ui/core';
import { BookmarkTreeNode, BookmarkChangesArg } from 'src/api/Manager';

const styles = (theme: Theme) => ({
  root: {
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    position: 'absolute' as 'absolute',
    width: theme.spacing(50),

    backgroundColor: theme.palette.background.paper,
    boxShadow: theme.shadows[5],
    outline: 'none',

    padding: theme.spacing(4),
    marginLeft: theme.spacing(3),
    marginRight: theme.spacing(3),
  },
  fieldSet: {
    width: '100%',
  },
  textField: {
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
  },
  buttons: {
    display: 'flex',
    marginTop: theme.spacing(4),
    justifyContent: 'flex-end'
  },
  button: {
    marginLeft: theme.spacing(1),
  },
  header: {
    ...theme.typography.h4,
    marginBottom: theme.spacing(2),
  }
});

type EditBoormarkType = 'new' | 'edit';

export interface OwnProps {
  item: BookmarkTreeNode;
  onClose: () => void;
  onSubmit: ({ title, url }: BookmarkChangesArg) => void;
  type?: EditBoormarkType;
}

type ClassKey = 'root' | 'fieldSet' | 'textField' | 'buttons' | 'button' | 'header';

export function EditBoormark({ item, classes, onClose, onSubmit, type = 'edit' }: OwnProps & WithStyles<ClassKey>) {
  const [title, setTitle] = React.useState(item.title);
  const [url, setUrl] = React.useState(item.url);

  const onSave = (e: React.SyntheticEvent) => {
    e.preventDefault();
    onSubmit({ title, url });
    onClose();
  };

  const onCancel = () => onClose();

  return (
    <Paper
      className={classes.root}
      // @ts-ignore
      component="form"
      role="dialog"
      onSubmit={onSave}
    >
      <FormControl
        className={classes.fieldSet}
        // @ts-ignore
        component="fieldset"
      >
        <FormLabel
          // @ts-ignore
          component="legend"
        >
          <Typography
            className={classes.header}
            variant="h1"
          >
            <Title type={type} />
          </Typography>
        </FormLabel >

        <FormControl
          margin="normal"
          fullWidth
        >
          <InputLabel
            htmlFor="name"
          >
            Name
          </InputLabel>
          <Input
            id="name"
            name="name"
            value={title}
            onChange={(e) => setTitle(e.target.value)}
            autoFocus
          />
        </FormControl>

        <FormControl
          margin="normal"
          fullWidth
        >
          <InputLabel
            htmlFor="url"
          >
            Url
          </InputLabel>
          <Input
            id="url"
            name="url"
            value={url}
            onChange={(e) => setUrl(e.target.value)}
            required
          />
        </FormControl>

        <div className={classes.buttons}>
          <Button
            className={classes.button}
            variant="outlined"
            color="primary"
            onClick={onCancel}
            size="small"
          >
            Cancel
          </Button>
          <Button
            className={classes.button}
            type="submit"
            variant="contained"
            color="primary"
            size="small"
          >
            Save
          </Button>
        </div>
      </FormControl >
    </Paper >
  );
}

function Title({ type }: { type: EditBoormarkType }) {
  switch (type) {
    case 'new': return <>New Bookmark</>;
    case 'edit': return <>Edit Bookmark</>;
    default:
      console.warn(`Unknown EditBookamrk modal type: ${type}`);
      return null;
  }
}

export default withStyles(styles)(EditBoormark);
