import * as React from 'react';
import {
  Paper,
  FormControl,
  FormLabel,
  Typography,
  Theme,
  WithStyles,
  withStyles,
  InputLabel,
  Input,
  Button,
} from '@material-ui/core';

import { BookmarkTreeNode, BookmarkChangesArg } from 'src/api/Manager';

const styles = (theme: Theme) => ({
  root: {
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    position: 'absolute' as 'absolute',
    width: theme.spacing(50),

    backgroundColor: theme.palette.background.paper,
    boxShadow: theme.shadows[5],
    outline: 'none',

    padding: theme.spacing(4),
    marginLeft: theme.spacing(3),
    marginRight: theme.spacing(3),
  },
  fieldSet: {
    width: '100%',
  },
  textField: {
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
  },
  buttons: {
    display: 'flex',
    marginTop: theme.spacing(4),
    justifyContent: 'flex-end'
  },
  button: {
    marginLeft: theme.spacing(1),
  },
  header: {
    ...theme.typography.h4,
    marginBottom: theme.spacing(2),
  }
});

export interface OwnProps {
  section: BookmarkTreeNode;
  onClose: () => void;
  onSubmit: ({ title }: BookmarkChangesArg) => void;
}

type ClassKey = 'root' | 'fieldSet' | 'textField' | 'buttons' | 'button' | 'header';

export function RenameSection({ classes, onClose, onSubmit, section }: OwnProps & WithStyles<ClassKey>) {
  const [title, setTitle] = React.useState(section.title);

  const onCancel = () => onClose();

  const onSave = (e: React.SyntheticEvent) => {
    e.preventDefault();
    onSubmit({ title });
    onClose();
  };

  return (
    <Paper
      className={classes.root}
      // @ts-ignore
      component="form"
      role="dialog"
      onSubmit={onSave}
    >
      <FormControl
        className={classes.fieldSet}
        component="fieldset"
      >
        <FormLabel
          component="legend"
        >
          <Typography
            className={classes.header}
            variant="h1"
          >
            Rename Group
          </Typography>
        </FormLabel>
      </FormControl>

      <FormControl
        margin="normal"
        fullWidth
      >
        <InputLabel htmlFor="name">Name</InputLabel>
        <Input
          id="name"
          name="name"
          value={title}
          onChange={(e) => setTitle(e.target.value)}
          autoFocus
        />
      </FormControl>

      <div className={classes.buttons}>
        <Button
          className={classes.button}
          variant="outlined"
          color="primary"
          onClick={onCancel}
          size="small"
        >
          Cancel
        </Button>
        <Button
          className={classes.button}
          type="submit"
          variant="contained"
          color="primary"
          size="small"
        >
          Save
        </Button>
      </div>
    </Paper>
  );
}

export default withStyles(styles)(RenameSection);
