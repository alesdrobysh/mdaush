import * as React from 'react';
// @ts-ignore
import Masonry from 'react-masonry-css';
import { withStyles, WithStyles, Theme } from '@material-ui/core/styles';
import { Menu, MenuItem, Modal, Typography } from '@material-ui/core';

import { BookmarkTreeNode, BookmarkChangesArg, BookmarkCreateArg } from 'src/api/Manager';
import SectionItem from 'src/components/SectionItem';
import RenameSection from './RenameSection';
import DeleteSectionDialog from './DeleteSectionDialog';
import EditBookmark from './EditBookmark';

const styles = (theme: Theme) => ({
  masonry: {
    display: 'flex',
    margin: `0 -${theme.spacing(1)}px`,
  },
  title: {
    color: theme.palette.common.white,
    textShadow: '0px 1px 5px rgba(0, 0, 0, 0.2), 0px 2px 2px rgba(0, 0, 0, 0.14), 0px 3px 1px rgba(0, 0, 0, 0.12)',
    fontSize: '3rem',
  },
  more: {
    alignSelf: 'flex-start',
    margin: `-${theme.spacing(1 / 4)}px`,
    padding: theme.spacing(1 / 4),
    position: 'relative' as 'relative',
    right: `-${theme.spacing(3 / 4)}px`,
  },
  moreInner: {
    height: `${theme.spacing(10 / 4)}px`,
    width: `${theme.spacing(10 / 4)}px`,
  }
});

const breakpointColumns = {
  default: 3,
  960: 2,
  600: 1,
};

export interface Props {
  section: BookmarkTreeNode;
  removeBookmark: (id: string) => Promise<void>;
  createBookmark: (changes: BookmarkCreateArg) => Promise<void>;
  removeTree: (id: string) => Promise<void>;
  updateTreeNode: (id: string, changes: BookmarkChangesArg) => Promise<void>;
}

type ClassKey = 'masonry' | 'title' | 'more' | 'moreInner' | 'flex';

function Section({
  section,
  classes,
  removeBookmark,
  removeTree,
  updateTreeNode,
  createBookmark,
}: Props & WithStyles<ClassKey>) {
  const [anchorEl, setAnchorEl] = React.useState<Element | null>(null);
  const [isRenameOpened, setIsRenameOpened] = React.useState(false);
  const [isAddBookmarkOpened, setIsAddBookmarkOpened] = React.useState(false);
  const [isDeleteDialogOpened, setIsDeleteDialogOpened] = React.useState(false);

  const onContextMenu = (e: React.SyntheticEvent) => {
    e.preventDefault();
    setAnchorEl(e.currentTarget);
  };

  const onClose = () => setAnchorEl(null);

  const onRenameClick = () => {
    setIsRenameOpened(true);
    onClose();
  };

  const onRenameModalSave = ({ title }: BookmarkChangesArg) => updateTreeNode(section.id, { title });

  const onRenameModalClose = () => setIsRenameOpened(false);

  const onDeleteClick = () => {
    setIsDeleteDialogOpened(true);
    onClose();
  };

  const onDeleteDialogOk = () => {
    removeTree(section.id);
    setIsDeleteDialogOpened(false);
  };

  const onDeleteDialogClose = () => setIsDeleteDialogOpened(false);

  const onAddBookmarkClick = () => {
    setIsAddBookmarkOpened(true);
    onClose();
  };

  const onAddBookmarkSave = (changeInfo: BookmarkChangesArg) => {
    createBookmark({ ...changeInfo, parentId: section.id });
    setIsAddBookmarkOpened(false);
  };

  const onAddBookmarkClose = () => setIsAddBookmarkOpened(false);

  return (
    <React.Fragment>
      <div>
        <Typography
          variant="h1"
          className={classes.title}
          onContextMenu={onContextMenu}
        >
          {section.title}
        </Typography>
        <Menu
          id="card-menu"
          anchorEl={anchorEl as HTMLElement}
          open={Boolean(anchorEl)}
          onClose={onClose}
        >
          <MenuItem onClick={onRenameClick}>Rename</MenuItem>
          <MenuItem onClick={onDeleteClick}>Delete</MenuItem>
          <MenuItem onClick={onAddBookmarkClick}>Add Bookmark</MenuItem>
        </Menu>
      </div>
      <Masonry
        className={classes.masonry}
        breakpointCols={breakpointColumns}
      >
        {
          section &&
          section.children &&
          section.children.map((child) => (
            <SectionItem
              remove={removeBookmark}
              update={updateTreeNode}
              key={child.id}
              item={child}
            />
          ))
        }
      </Masonry>

      <Modal
        open={isRenameOpened}
        onClose={onRenameModalClose}
      >
        <RenameSection
          section={section}
          onClose={onRenameModalClose}
          onSubmit={onRenameModalSave}
        />
      </Modal>

      <Modal
        open={isAddBookmarkOpened}
        onClose={onAddBookmarkClose}
      >
        <EditBookmark
          type="new"
          item={{ title: '', id: '' }}
          onClose={onAddBookmarkClose}
          onSubmit={onAddBookmarkSave}
        />
      </Modal>

      <DeleteSectionDialog
        isOpened={isDeleteDialogOpened}
        onClose={onDeleteDialogClose}
        onOk={onDeleteDialogOk}
      />
    </React.Fragment >
  );
}

export default withStyles(styles)(Section);
