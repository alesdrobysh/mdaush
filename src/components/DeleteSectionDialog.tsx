import * as React from 'react';

import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

type DeleteSectionDialogProps = {
  isOpened: boolean;
  onClose: () => void;
  onOk: () => void;
};

function DeleteSectionDialog({ isOpened, onOk, onClose }: DeleteSectionDialogProps) {
  return (
    <Dialog
      open={isOpened}
      onClose={onClose}
      aria-labelledby="alert-dialog-title"
      aria-describedby="alert-dialog-description"
    >
      <DialogTitle id="alert-dialog-title">Remove Section</DialogTitle>
      <DialogContent>
        <DialogContentText id="alert-dialog-description">
          You are about to remove the bookmarks section. Are you sure?
        </DialogContentText>
      </DialogContent>
      <DialogActions>
        <Button onClick={onClose} color="primary">
          Cancel
          </Button>
        <Button onClick={onOk} color="primary" autoFocus>
          Ok
          </Button>
      </DialogActions>
    </Dialog>
  );
}

export default DeleteSectionDialog;
