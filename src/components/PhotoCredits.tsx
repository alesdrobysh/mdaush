import * as React from 'react';
import { withStyles, WithStyles, Theme } from '@material-ui/core/styles';

import { Photo } from 'src/localStorage';

export interface Props {
  user?: Photo['user'];
}

const styles = (theme: Theme) => ({
  root: {
    display: 'flex',
    alignItems: 'center',
    paddingLeft: theme.spacing(2),
    paddingBottom: theme.spacing(2),
    color: '#fff',
  },
  link: {
    color: '#fff',
    textDecoration: 'underline',
  },
});

type ClassNames = 'root' | 'link';

function BottomPanel({ user, classes }: Props & WithStyles<ClassNames>) {
  if (!user) {
    return null;
  }

  const linkToPhotographer = `${user.links.html}?utm_source=mdaush&utm_medium=referral`;
  const photographerName = `${user.first_name} ${user.last_name}`;
  const linkToUnsplash = 'https://unsplash.com/?utm_source=mdaush&utm_medium=referral';

  const { root, link } = classes;

  return (
    <div className={root}>
      <span>
        Photo by
      {'\u00A0'}
        <a className={link} href={linkToPhotographer}>{photographerName}</a>
        {'\u00A0'}
        on
      {'\u00A0'}
        <a className={link} href={linkToUnsplash}>Unsplash</a>
      </span>
    </div>
  );
}

export default withStyles(styles)(BottomPanel);
