import * as React from 'react';
import { withStyles, WithStyles, Theme } from '@material-ui/core/styles';
export interface Props {
  photoCredits: JSX.Element;
}

const styles = (theme: Theme) => ({
  root: {
    position: 'fixed' as 'fixed',
    bottom: 0,
    left: 0,
    right: 0,
    display: 'flex',
    justifyContent: 'space-between',
  },
  icon: {
    fill: '#fff',
  }
});

type ClassNames = 'root';

function BottomPanel({ photoCredits, classes }: Props & WithStyles<ClassNames>) {
  const { root } = classes;

  // tslint:disable-next-line: no-console
  console.log(photoCredits);

  return (
    <div className={root}>
      {photoCredits}
    </div>
  );
}

export default withStyles(styles)(BottomPanel);
