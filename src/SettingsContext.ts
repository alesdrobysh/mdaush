import * as React from 'react';

export interface Settings {
  editMode: boolean;
}

export interface SettingsContextShape {
  settings: Settings;
  toggleEditMode: () => void;
}

const initialSettings: Settings = {
  editMode: false,
};

export const SettingsContext = React.createContext({
  settings: initialSettings,
  toggleEditMode: () => { /* nothing to do yet */ },
});
