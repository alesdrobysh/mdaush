import * as React from 'react';
import immer from 'immer';
import { Theme, withStyles, WithStyles } from '@material-ui/core';
// @ts-ignore
import Masonry from 'react-masonry-css';

import Manager, { BookmarkTreeNode, BookmarkChangesArg } from 'src/api/Manager';
import ManagerMock from 'src/api/ManagerMock';
import BottomPanel from 'src/components/BottomPanel';
import Section from 'src/components/Section';

import { SettingsContext, SettingsContextShape } from 'src/SettingsContext';

export interface Props {
  photoCredits: JSX.Element;
}
export interface State {
  bookmarks: BookmarkTreeNode[];
  settingsContext: SettingsContextShape;
}

const styles = (theme: Theme) => ({
  root: {
    padding: `${theme.spacing(1)}px ${theme.spacing(2)}px ${theme.spacing(4)}px`,
  },
  masonry: {
    display: 'flex',
    margin: `-${theme.spacing(3 / 2)}px -${theme.spacing(2)}px`,
  },
  section: {
    margin: theme.spacing(2),
  }
});

type ClassKey = 'root' | 'masonry' | 'section';

class App extends React.Component<Props & WithStyles<ClassKey>, State> {
  private manager: Manager;
  private breakpointColumns = {
    default: 2,
    600: 1,
  };

  constructor(props: Props & WithStyles<ClassKey>) {
    super(props);
    if (process.env.NODE_ENV === 'production') {
      this.manager = new Manager();
    } else {
      // @ts-ignore
      this.manager = new ManagerMock() as Manager;
    }

    this.state = {
      bookmarks: [],
      settingsContext: {
        settings: {
          editMode: false,
        },
        toggleEditMode: this.toggleEditMode,
      },
    };
  }

  async componentDidMount() {
    await this.manager.init();
    this.fetchBookmarks();
  }

  fetchBookmarks = () => {
    const bookmarks = this.manager.loadBookmarks();
    this.setState({
      bookmarks,
    });
  }

  toggleEditMode = () => {
    const newState = immer(this.state, (draft) => {
      draft.settingsContext.settings.editMode = !draft.settingsContext.settings.editMode;
    });

    this.setState(newState);
  }

  removeBookmark = async (id: string) => {
    await this.manager.remove(id);
    this.fetchBookmarks();
  }

  createBookmark = async (changes: BookmarkChangesArg) => {
    await this.manager.create(changes);
    this.fetchBookmarks();
  }

  removeTree = async (id: string) => {
    await this.manager.removeTree(id);
    this.fetchBookmarks();
  }

  updateTreeNode = async (id: string, changes: BookmarkChangesArg) => {
    await this.manager.update(id, changes);
    this.fetchBookmarks();
  }

  render() {
    const { photoCredits, classes } = this.props;
    const { bookmarks } = this.state;
    return (
      <SettingsContext.Provider value={this.state.settingsContext}>
        <div className={classes.root}>
          <Masonry
            breakpointCols={this.breakpointColumns}
            className={classes.masonry}
          >
            {
              bookmarks.map(section => (
                <div
                  className={classes.section}
                  key={section.id}
                >
                  <Section
                    removeBookmark={this.removeBookmark}
                    createBookmark={this.createBookmark}
                    removeTree={this.removeTree}
                    updateTreeNode={this.updateTreeNode}
                    section={section}
                  />
                </div>
              )
              )
            }
          </Masonry>
          <BottomPanel photoCredits={photoCredits} />
        </div>
      </SettingsContext.Provider>
    );
  }
}

export default withStyles(styles)(App);
